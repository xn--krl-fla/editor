1. npm i
2. npm run tailwind
3. open index.html
4. change commands(or leave unchanged) with `remove` and `add` buttons
5. select command(by clicking on it), press `collect poses`. Wait 3 seconds, `collecting` will appear, do the pose for 5 seconds
6. repeat for every command (or at least for 2 of them)
7. press Train and wait
8. do a pose and press `write code`. The code block will be rewritten to main code.