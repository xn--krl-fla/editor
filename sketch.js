// ml5.js: Pose Estimation with PoseNet
// The Coding Train / Daniel Shiffman
// https://thecodingtrain.com/Courses/ml5-beginners-guide/7.1-posenet.html
// https://youtu.be/OIo-DIOkNVg
// https://editor.p5js.org/codingtrain/sketches/ULA97pJXR

/*
DONE: Commands as cards
DONE: Adding/removing commands
TODO: Selected command to train / detected command
TODO: Use actually detected command
DONE: remove button hover
TODO: Brain custom number of outputs
TODO: status of recording/training
TODO: ? move collect poses to individual code blocks
*/

hljs.highlightAll();

let video;
let poseNet;
let pose;
let skeleton;

let brain;
let poseLabel = "";

let state; //editing||collecting||training||classifying
let changeState = (to) => {
  console.log(to);
  document.getElementById("out").innerHTML = to;
  document.getElementById("main-container").classList.remove(state);
  document.getElementById("main-container").classList.add(to);
  state = to;
}
changeState("editing");
let commands = [{command: "print('yeet')\n"}, {command: "input()\n"},{command: "x = 0\n"},{command: "print(x)\n"}];



let addCommandToDOM = function (command,index){
  //hej hej jasne ze to nabuduce fakin spravim cez templaty omg
  let container = document.createElement("div");
  container.classList.add("command-container")
  container.addEventListener("click",changeCommand(container));
  let removeButton = document.createElement("button");
  removeButton.innerHTML = "remove";
  removeButton.addEventListener("click",removeCommand(container));
  removeButton.classList.add("remove-command");
  container.append(removeButton);
  let cmd = document.createElement("code");
  cmd.innerHTML = command;
  cmd.classList.add("language-python", "command");
  let pre = document.createElement("pre");
  hljs.highlightElement(cmd);
  pre.append(cmd);
  container.append(pre);
  document.getElementById("commands").append(container);
}
let changeCommand = (container) => (event) => {
  activeCommand!==-1&&container.parentElement.childNodes[activeCommand].classList.remove("highlighted");
  container.classList.add("highlighted");
  activeCommand = getElementIndex(container);
  document.getElementById("actuallyTrained").innerHTML = String(activeCommand);
}
let removeCommand = (container) => (event) => {
  container.remove();
  event.stopPropagation();
}
commands.forEach(({command},i) => addCommandToDOM(command,i))

function getElementIndex (element) {
  return Array.from(element.parentNode.children).indexOf(element);
}
let activeCommand = -1;

let editButton = document.getElementById("edit");
let collectButton = document.getElementById("collect");
let trainButton = document.getElementById("train");
let writeCodeButton = document.getElementById("write-code");
let addCommandButton = document.getElementById("add-command");

editButton.addEventListener("click",startEditing);
collectButton.addEventListener("click",startCollecting);
trainButton.addEventListener("click",startTraining);
writeCodeButton.addEventListener("click",useCommand);
addCommandButton.addEventListener("click",() =>addCommandToDOM(window.prompt("Prikaz?","yeet()")))

let recognizedCommand = -1;

function useCommand() {
  console.log(recognizedCommand);
  recognizedCommand!==-1&&document.getElementById("commands").childNodes[recognizedCommand].classList.remove("recognized");
  document.getElementById("commands").classList.add("recognized");

  document.getElementById("main-code").innerHTML+=commands[+recognizedCommand].command;
  document.getElementById("main-code").highlightElement();
}

function classify() {
  if (pose) {
    let inputs = [];
    for (let i = 0; i < pose.keypoints.length; i++) {
      let x = pose.keypoints[i].position.x;
      let y = pose.keypoints[i].position.y;
      inputs.push(x);
      inputs.push(y);
    }
    brain.classify(inputs, gotResult);
  } else {
    //setTimeout(classify, 100);
  }
}
function gotResult(error,results) {
  console.log(results[0]);
  recognizedCommand = +results[0].label;
  //classify();
}
function startEditing(){

  changeState("editing");
  collectButton.disabled = false;
  trainButton.disabled = true;
  writeCodeButton.disabled = true;


}
function startCollecting() {
  trainButton.disabled = false;
  writeCodeButton.disabled = true;

  setTimeout(function() {
    changeState("collecting");
    console.log(activeCommand);
    setTimeout(function() {
      console.log('not collecting');
      changeState("editing");
    }, 5000);
  }, 3000);
}
function startTraining(){
  writeCodeButton.disabled = false;

  changeState("training");
  brain.normalizeData();
  brain.train({epochs:20}, finished);
}
function finished() {
  console.log("model trained");
  changeState("classifying");
  //brain.save();
}

function modelLoaded() {
  console.log('poseNet ready');
}

let options = {
  inputs: 34,
  outputs: 4,
  task: 'classification',
  debug: true
}

function setup() {
  frameRate(3);
  let cnv = createCanvas(640, 480);
  cnv.parent("canvas-container");
  video = createCapture(VIDEO);
  video.hide();
  poseNet = ml5.poseNet(video, modelLoaded);
  poseNet.on('pose', gotPoses);

  brain = ml5.neuralNetwork(options);
}
function draw() {
  translate(video.width, 0);
  scale(-1, 1);
  image(video, 0, 0, video.width, video.height);

  if (pose) {
    for (let i = 0; i < skeleton.length; i++) {
      let a = skeleton[i][0];
      let b = skeleton[i][1];
      strokeWeight(2);
      stroke(0);

      line(a.position.x, a.position.y, b.position.x, b.position.y);
    }
    for (let i = 0; i < pose.keypoints.length; i++) {
      let x = pose.keypoints[i].position.x;
      let y = pose.keypoints[i].position.y;
      fill(0);
      stroke(255);
      ellipse(x, y, 16, 16);
    }
  }
  if(state==="classifying"){
    classify();
  }
}



function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose;
    skeleton = poses[0].skeleton;
    if (state === 'collecting') {
      let inputs = [];
      for (let i = 0; i < pose.keypoints.length; i++) {
        let x = pose.keypoints[i].position.x;
        let y = pose.keypoints[i].position.y;
        inputs.push(x);
        inputs.push(y);
      }
      console.log("added input");
      let target = [String(activeCommand)];
      brain.addData(inputs, target);
    }
  }
}
